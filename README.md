barBundle
=======================

Installation
------------

### Step 1: Add repository in composer.json

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://mr_Verman@bitbucket.org/yapa89/barbundle.git"
    }
  ]
}
```

### Step 2: Run update
```bash
composer require yapa89/barbundle:^1.0.0
```

### Step 3: Enable the Bundle
```php
<?php
// config/bundles.php

return [
     // .... 
     Yapa89\BarBundle\BarBundle::class => ['all' => true],
];
```

