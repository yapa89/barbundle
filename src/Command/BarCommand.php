<?php

namespace Yapa89\BarBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BarCommand extends Command
{
    public function configure()
    {
        $this->setName('bar:hi');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $output->writeln('Hi from Bar!');
        return Command::SUCCESS;
    }

}
