<?php

/*
 * Bar Bundle
 *
 * (c) Pavel Yankovets <yanko.en@gmail.com>
 */

declare(strict_types=1);

namespace Yapa89\BarBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BarBundle extends Bundle
{
}
